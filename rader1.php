<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 11:23 */

// 雷达图

require './src/jpgraph.php';
require './src/jpgraph_radar.php';
require './src/jpgraph_log.php';


// Create the basic radar graph
$graph = new RadarGraph(300,200);
//$graph->img->SetAntiAliasing();

// Set background color and shadow
$graph->SetColor("white");
$graph->SetShadow();

// Position the graph
$graph->SetCenter(0.4,0.55);

// Setup the axis formatting
$graph->axis->SetFont(FF_FONT1,FS_BOLD);

// Setup the grid lines
$graph->grid->SetLineStyle("solid");
$graph->grid->SetColor("navy");
$graph->grid->Show();
$graph->HideTickMarks();

// Setup graph titles
$graph->title->Set("Quality result");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->SetTitles($gDateLocale->GetShortMonth());

// Create the first radar plot
$plot = new RadarPlot(array(70,80,60,90,71,81,47));
$plot->SetLegend("Goal");
$plot->SetColor("red","lightred");
$plot->SetFill(false);
$plot->SetLineWeight(2);

// Create the second radar plot
$plot2 = new RadarPlot(array(70,40,30,80,31,51,14));
$plot2->SetLegend("Actual");
$plot2->SetLineWeight(2);
$plot2->SetColor("blue");
$plot2->SetFill(false);

// Add the plots to the graph
$graph->Add($plot2);
$graph->Add($plot);

// And output the graph
$graph->Stroke();





/*
// Create the basic radar graph
$graph = new RadarGraph(300,200);
$graph->img->SetAntiAliasing();

// Set background color and shadow
$graph->SetColor("white");
$graph->SetShadow();

// Position the graph
$graph->SetCenter(0.4,0.55);

// Setup the axis formatting
$graph->axis->SetFont(FF_FONT1,FS_BOLD);

// Setup the grid lines
$graph->grid->SetLineStyle("solid");
$graph->grid->SetColor("navy");
$graph->grid->Show();
$graph->HideTickMarks();

// Setup graph titles
$graph->title->Set("Quality result");
$graph->title->SetFont(FF_FONT1,FS_BOLD);

$graph->SetTitles($gDateLocale->GetShortMonth());

// Create the first radar plot
$plot = new RadarPlot(array(70,80,60,90,71,81,47));
$plot->SetLegend("Goal");
$plot->SetColor("red","lightred");
$plot->SetFill(false);
$plot->SetLineWeight(2);

// Create the second radar plot
$plot2 = new RadarPlot(array(70,40,30,80,31,51,14));
$plot2->SetLegend("Actual");
$plot2->SetLineWeight(2);
$plot2->SetColor("blue");
$plot2->SetFill(false);

// Add the plots to the graph
$graph->Add($plot2);
$graph->Add($plot);

// And output the graph
$graph->Stroke();

*/

/*
// Create the basic rtadar graph
$graph = new RadarGraph(300,200);

// Set background color and shadow
$graph->SetColor("white");
$graph->SetShadow();

// Position the graph
$graph->SetCenter(0.4,0.55);

// Setup the axis formatting
$graph->axis->SetFont(FF_FONT1,FS_BOLD);
$graph->axis->SetWeight(2);

// Setup the grid lines
$graph->grid->SetLineStyle("longdashed");
$graph->grid->SetColor("navy");
$graph->grid->Show();
$graph->HideTickMarks();

// Setup graph titles
$graph->title->Set("Quality result");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->SetTitles(array("One","Two","Three","Four","Five","Sex","Seven","Eight","Nine","Ten"));
// Create the first radar plot
$plot = new RadarPlot(array(30,80,60,40,71,81,47));
$plot->SetLegend("Goal");
$plot->SetColor("red","lightred");
$plot->SetFill(false);
$plot->SetLineWeight(2);

// Create the second radar plot
$plot2 = new RadarPlot(array(70,40,30,80,31,51,14));
$plot2->SetLegend("Actual");
$plot2->SetColor("blue","lightred");

// Add the plots to the graph
$graph->Add($plot2);
$graph->Add($plot);

// And output the graph
$graph->Stroke();
*/



/*
// Some data to plot
$data = array(242,58,1500,12,1397,810,373);
$data2 = array(447,176,1472,191,1616,42,46);

// Create the graph
$graph = new RadarGraph(300,350);

// Use logarithmic scale (If you don't use any SetScale()
// the radar graph will default to linear scale
$graph->SetScale('log');

$graph->title->SetFont(FF_ARIAL,FS_BOLD,16);
$graph->title->Set('Logarithmic scale');
$graph->title->SetMargin(10);

// Make the radar graph fill out it's bounding box
$graph->SetPlotSize(0.8);
$graph->SetCenter(0.5,0.55);

// Uncomment the following line if you want to supress
// minor tick marks
//$graph->yscale->ticks->SupressMinorTickMarks();

// We want the major tick marks to be black and minor
// slightly less noticable
$graph->yscale->ticks->SetMarkColor('black','darkgray');

// Set the axis title font
$graph->axis->title->SetFont(FF_ARIAL,FS_BOLD,14);
$graph->axis->title->SetColor('darkred:0.8');

// Use blue axis
$graph->axis->SetColor('blue');

$plot = new RadarPlot($data);
$plot->SetLineWeight(1);
$plot->SetColor('forestgreen');
$plot->SetFillColor('forestgreen@0.9');

$plot2 = new RadarPlot($data2);
$plot2->SetLineWeight(2);
$plot2->SetColor('red');
$plot2->SetFillColor('red@0.9');

// Add the plot and display the graph
$graph->Add($plot);
$graph->Add($plot2);
$graph->Stroke();
*/



/*

// Some data to plot
$data = array(242,58,1500,12,1397,810,373);
$data2 = array(447,176,1472,191,1616,42,46);

// Create the graph
$graph = new RadarGraph(300,350);

// Use logarithmic scale (If you don't use any SetScale()
// the radar graph will default to linear scale
$graph->SetScale('log');

$graph->title->SetFont(FF_ARIAL,FS_BOLD,16);
$graph->title->Set('Logarithmic scale');
$graph->title->SetMargin(10);

// Make the radar graph fill out it's bounding box
$graph->SetPlotSize(0.8);
$graph->SetCenter(0.5,0.55);

// Note: Enabling this results in a very noticable slow
// down of the image generation! And more load on your
// server.
$graph->img->SetAntiAliasing();

// Uncomment the following line if you want to supress
// minor tick marks
//$graph->yscale->ticks->SupressMinorTickMarks();

// We want the major tick marks to be black and minor
// slightly less noticable
$graph->yscale->ticks->SetMarkColor('black','darkgray');

// Set the axis title font
$graph->axis->title->SetFont(FF_ARIAL,FS_BOLD,14);
$graph->axis->title->SetColor('darkred:0.8');

// Use blue axis
$graph->axis->SetColor('blue');

$plot = new RadarPlot($data);
$plot->SetLineWeight(1);
$plot->SetColor('forestgreen');
$plot->SetFillColor('forestgreen@0.9');

$plot2 = new RadarPlot($data2);
$plot2->SetLineWeight(2);
$plot2->SetColor('red');
$plot2->SetFillColor('red@0.9');

// Add the plot and display the graph
$graph->Add($plot);
$graph->Add($plot2);
$graph->Stroke();*/