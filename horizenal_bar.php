<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 10:31 */

/*
 * 水平直方图
 * */

require './src/jpgraph.php';
require './src/jpgraph_bar.php';

$datay = array(1992,1993,1995,1996,1997,1998,2001);

// Size of graph
$width=400;
$height=500;

// Set the basic parameters of the graph
$graph = new Graph($width,$height);
$graph->SetScale('textlin');

$top = 60;
$bottom = 30;
$left = 80;
$right = 30;
$graph->Set90AndMargin($left,$right,$top,$bottom);

// Nice shadow
$graph->SetShadow();

// Setup labels
$lbl = array("Andrew\nTait","Thomas\nAnderssen","Kevin\nSpacey","Nick\nDavidsson",
    "David\nLindquist","Jason\nTait","Lorin\nPersson");
$graph->xaxis->SetTickLabels($lbl);

// Label align for X-axis
$graph->xaxis->SetLabelAlign('right','center','right');

// Label align for Y-axis
$graph->yaxis->SetLabelAlign('center','bottom');

// Titles
$graph->title->Set('Number of incidents');

// Create a bar pot
$bplot = new BarPlot($datay);
$bplot->SetFillColor('orange');
$bplot->SetWidth(0.5);
$bplot->SetYMin(1990);

$graph->Add($bplot);

$graph->Stroke();