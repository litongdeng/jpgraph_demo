<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 16:36 */
require_once ('./src/jpgraph.php');
require_once ('./src/jpgraph_bar.php');
require_once ('./src/jpgraph_date.php');
require_once ('./src/jpgraph_mgraph.php');

DEFINE('BKG_COLOR','green:1.98');
DEFINE('WIND_HEIGHT',800);
DEFINE('WIND_WIDTH',250);

$data1y=array(0,8,9,3,5,6);
$data2y=array(18,2,1,7,5,4);

// Create the graph. These two calls are always required
$width = 500;
$height = 400;
$graph = new Graph($width,$height);
$graph->SetScale("textlin");

$graph->SetShadow();
$graph->img->SetMargin(40,30,20,40);//设置图形的边距

// Create the bar plots
$b1plot = new BarPlot($data1y);
$b1plot->SetFillColor("orange");
$b1plot->value->Show();
$b2plot = new BarPlot($data2y);
$b2plot->SetFillColor("blue");
$b2plot->value->Show();

// Create the grouped bar plot
$gbplot = new AccBarPlot(array($b1plot,$b2plot));

// ...and add it to the graPH
$graph->Add($gbplot);
//设置标题字体样式

$title = "中文标题";
$xTitle = "X-title";
$yTitle = "Y-title";

$graph->title->Set(iconv("UTF-8","GB2312//IGNORE", $title));
$graph->xaxis->title->Set(iconv("UTF-8","GB2312//IGNORE",$xTitle));
$graph->yaxis->title->Set(iconv("UTF-8","GB2312//IGNORE",$yTitle));

$graph->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->yaxis->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->xaxis->title->SetFont(FF_SIMSUN,FS_BOLD);




//-----------------------
// Create a multigraph
//----------------------
$mgraph = new MGraph();
$mgraph->SetMargin(2,2,2,2);
$mgraph->SetFrame(true,'darkgray',2);
$mgraph->SetFillColor(BKG_COLOR);

$mgraph->Add($graph,0,0);
$mgraph->Add($graph,$width,0);

$mgraph->title->Set('Climate diagram 12 March 2009');
$mgraph->title->SetFont(FF_ARIAL,FS_BOLD,20);
$mgraph->title->SetMargin(8);
$mgraph->Stroke();

