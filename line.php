<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 10:11 */

require './src/jpgraph.php';
require './src/jpgraph_line.php';

// Some data
$ydata = array(11,3,8,12,5,1,9,13,5,7,20);

// Create the graph. These two calls are always required
$graph = new Graph(350,250);
$graph->SetScale('textlin');

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor('blue');

// Add the plot to the graph
$graph->Add($lineplot);

// Display the graph
$graph->Stroke();
