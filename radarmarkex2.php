<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 10:44 */

/*
 * 能力水平雷达图
 * */

require './src/jpgraph.php';
require './src/jpgraph_radar.php';

$titles=array('Planning','Quality','Time','RR');
$data=array(60, 100, 100, 64);

$graph = new RadarGraph (300 * 2,280 * 2);

$graph->SetScale('lin',0,100);
$graph->yscale->ticks->Set(20,40,40,60, 80);

$graph->title->Set('Radar with marks');
$graph->title->SetFont(FF_VERDANA,FS_NORMAL,12);

$graph->SetTitles($titles);
$graph->SetCenter(0.5,0.55);
$graph->HideTickMarks();
$graph->SetColor('lightgreen@0.7');

$graph->axis->SetColor('darkgray');

$graph->grid->SetColor('darkgray');
$graph->grid->Show();

$graph->axis->title->SetFont(FF_ARIAL,FS_NORMAL,12);
$graph->axis->title->SetMargin(5);

$graph->SetGridDepth(DEPTH_BACK);
$graph->SetSize(0.6);

$plot = new RadarPlot($data);
$plot->SetColor('red@0.2');
$plot->SetLineWeight(3);
//$plot->SetFillColor('red@0.7');

//$plot->mark->SetType(MARK_IMG_SBALL,'red');

$graph->Add($plot);
$graph->Stroke();