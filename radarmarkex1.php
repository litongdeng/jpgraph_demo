<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 10:44 */

/*
 * 知识点难度系数分布图
 * */

require './src/jpgraph.php';
require './src/jpgraph_radar.php';




// Some data to plot
$data = array(242,58,1500,12,1397,810,373);
$data2 = array(447,176,1472,191,1616,42,46);

// Create the graph
$graph = new RadarGraph(300,350);

// Use logarithmic scale (If you don't use any SetScale()
// the radar graph will default to linear scale
$graph->SetScale('log');

$graph->title->SetFont(FF_ARIAL,FS_BOLD,16);
$graph->title->Set('Logarithmic scale');
$graph->title->SetMargin(10);

// Make the radar graph fill out it's bounding box
$graph->SetPlotSize(0.8);
$graph->SetCenter(0.5,0.55);

// Uncomment the following line if you want to supress
// minor tick marks
//$graph->yscale->ticks->SupressMinorTickMarks();

// We want the major tick marks to be black and minor
// slightly less noticable
$graph->yscale->ticks->SetMarkColor('black','darkgray');

// Set the axis title font
$graph->axis->title->SetFont(FF_ARIAL,FS_BOLD,14);
$graph->axis->title->SetColor('darkred:0.8');

// Use blue axis
$graph->axis->SetColor('blue');

$plot = new RadarPlot($data);
$plot->SetLineWeight(1);
$plot->SetColor('forestgreen');
$plot->SetFillColor('forestgreen@0.9');

$plot2 = new RadarPlot($data2);
$plot2->SetLineWeight(2);
$plot2->SetColor('red');
$plot2->SetFillColor('red@0.9');

// Add the plot and display the graph
$graph->Add($plot);
$graph->Add($plot2);
$graph->Stroke();





// Create the basic radar graph
$graph = new RadarGraph(300,200);
$graph->img->SetAntiAliasing();

// Set background color and shadow
$graph->SetColor("white");
$graph->SetShadow();

// Position the graph
$graph->SetCenter(0.4,0.55);

// Setup the axis formatting
$graph->axis->SetFont(FF_FONT1,FS_BOLD);

// Setup the grid lines
$graph->grid->SetLineStyle("solid");
$graph->grid->SetColor("navy");
$graph->grid->Show();
$graph->HideTickMarks();

// Setup graph titles
$graph->title->Set("Quality result");
$graph->title->SetFont(FF_FONT1,FS_BOLD);

$graph->SetTitles($gDateLocale->GetShortMonth());

// Create the first radar plot
$plot = new RadarPlot(array(70,80,60,90,71,81,47));
$plot->SetLegend("Goal");
$plot->SetColor("red","lightred");
$plot->SetFill(false);
$plot->SetLineWeight(2);

// Create the second radar plot
$plot2 = new RadarPlot(array(70,40,30,80,31,51,14));
$plot2->SetLegend("Actual");
$plot2->SetLineWeight(2);
$plot2->SetColor("blue");
$plot2->SetFill(false);

// Add the plots to the graph
$graph->Add($plot2);
$graph->Add($plot);

// And output the graph
$graph->Stroke();

















$titles = array('Planning','Quality','Time','RR','CR','Planning','Quality','Time','RR','CR',);
$data = array(60, 100, 100, 64, 64,100, 64, 64, 100, 100);

$graph = new RadarGraph (300 * 2,280 * 2);
$graph->SetScale('lin',0,100);
$graph->yscale->ticks->Set(20,40,40,60, 80);

$graph->title->Set('Radar with marks');
$graph->title->SetFont(FF_VERDANA,FS_NORMAL,12);

$graph->SetTitles($titles);
$graph->SetCenter(0.5,0.55);
$graph->HideTickMarks();
$graph->SetColor('lightgreen@0.7');
$graph->axis->SetColor('darkgray');

$graph->grid->SetColor('darkgray');
$graph->grid->Show();

$graph->axis->title->SetFont(FF_ARIAL,FS_NORMAL,12);
$graph->axis->title->SetMargin(5);
$graph->SetGridDepth(DEPTH_BACK);
$graph->SetSize(0.6);

$plot = new RadarPlot($data);
$plot->SetColor('red@0.2');
$plot->SetLineWeight(1);
$plot->SetFillColor('red@0.7');

//$plot->mark->SetType(MARK_IMG_SBALL,'red');

$graph->Add($plot);
$graph->Stroke();