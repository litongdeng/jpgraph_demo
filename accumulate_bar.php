<?php
/* created by phpstorm9.0.2, author: denglitong, date-time: 2017/3/20 10:35 */
require_once ('./src/jpgraph.php');
require_once ('./src/jpgraph_bar.php');

$data1y=array(0,8,9,3,5,6);
$data2y=array(18,2,1,7,5,4);

// Create the graph. These two calls are always required
$graph = new Graph(500,400);
$graph->SetScale("textlin");

$graph->SetShadow();
$graph->img->SetMargin(40,30,20,40);

// Create the bar plots
$b1plot = new BarPlot($data1y);
$b1plot->SetFillColor("orange");
$b1plot->value->Show();
$b2plot = new BarPlot($data2y);
$b2plot->SetFillColor("blue");
$b2plot->value->Show();

// Create the grouped bar plot
$gbplot = new AccBarPlot(array($b1plot,$b2plot));

// ...and add it to the graPH
$graph->Add($gbplot);


$title = "信息统计表";
$xTitle = "X-title";
$yTitle = "Y-title";

function utf8togb(&$var) {
    if (is_array($var)) {
        foreach ($var as &$item) {
            utf8togb($item);
        }
    } else {
        $var = iconv("UTF-8","GB2312//IGNORE",$var);
    }
}
utf8togb($title);
//var_dump($title);
//return;
$graph->title->Set($title);
$graph->xaxis->title->Set(iconv("UTF-8","GB2312//IGNORE",$xTitle));
$graph->yaxis->title->Set(iconv("UTF-8","GB2312//IGNORE",$yTitle));

$graph->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->yaxis->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->xaxis->title->SetFont(FF_SIMSUN,FS_BOLD);

$graph->Stroke();